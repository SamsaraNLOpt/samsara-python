# Samsara

1.  [Documentation](#orgeb4c505)
2.  [Tutorial](#orgab3aa3a)
3.  [Credits](#org392e1bb)

Samsara is a reverse communication nonlinear optimization solver for smooth unconstrained objectives. Samsara is just an oracle that suggests a step (direction and length) using previous information provided to it by the calling routine. It does not execute function evaluations or gradient calculations, but it does build a model of the function being optimized, based on the steps, gradients and function values (if available) passed to it by the user. This repository contains the Python version of Samsara.

This package is available on PyPI and can be installed with the command

```sh
pip install samsara
```


<a id="orgeb4c505"></a>

## Documentation


<a id="orgab3aa3a"></a>

## Tutorial

The file `driver.py` contains a running example of how to use Samsara. This document tries to explain how all the moving pieces in the example fit together.

The first step is importing samsara

```python
import matplotlib.pyplot as plt
from numpy import abs, amax, array, load, zeros
from numpy.linalg import norm

from samsara import Samsara, BFGS1_product, Broyden1_product,\
  Broyden2_product, Dogleg_QN, Explicit_TR, cdSY_mat, fdSY_mat
```

In order to use Samsara, we need an objective function to minimize and its gradient. For this example we will use Engvall's function

```math
f(x, y) = x^4 + y^4 + 2x^2y^2 - 4x + 3.
```

```python
def Objective(x):
    f = x[0]**4 + x[1]**4 + 2*x[0]**2*x[1]**2 - 4*x[0] + 3
    Df = array([
	4*x[0]**3 + 4*x[0]*x[1]**2 - 4,
	4*x[1]**3 + 4*x[0]**2*x[1]
    ])
    return f, Df
```

The next step is to create the options dictionary that will be passed to the solver.

```python
ngrad_TOL = 6e-6   # eps**(1/3)
step_TOL = 4e-11   # eps**(2/3)
Maxit = 500
options = {}
options['verbose'] = True
options['alpha_0'] = 5e-12
options['gamma'] = .5
options['c'] = .01
options['beta'] = .9999999
options['eta1'] = .995
options['eta2'] = .8
options['eta3'] = .25
options['maxmem'] = 8
options['tr'] = 1e+15
options['ngrad_TOL'] = ngrad_TOL
options['step_TOL'] = step_TOL
options['Maxit'] = Maxit
options['QN_method'] = BFGS1_product
options['Step_finder'] = Explicit_TR
options['History'] = cdSY_mat
options['update_conditions'] = 'Trust Region'
options['initial_step'] = 1e+5*options['step_TOL']
```

We need to unpack this dictionary as parameters in the constructor of the `Samsara` class.

```python
samsara = Samsara(**options)
```

Now that the `Samsara` object was created we need to provide the initial configuration of the main loop.

```python
xold_vec = array([.5, 2.])
fold, gradfold_vec = Objective(xold_vec)
xnew_vec = None
fnew = None
gradfnew_vec = None
stepsize = 999.
ngradfnew = 999.
it = 0
```

The main loop of the program has to call the oracle in order to obtain a new point candidate. In this example we will accept the candidate unconditionally.

```python
while it < Maxit and ngradfnew > ngrad_TOL and stepsize > step_TOL:
    xnew_vec, xold_vec, fold, gradfold_vec, stepsize =\
	samsara.run(xold_vec, xnew_vec, fold, fnew, gradfold_vec,
		    gradfnew_vec)
    it += 1
    fnew, gradfnew_vec = Objective(xnew_vec)
    ngradfnew = norm(gradfnew_vec)
```

```
First iteration: initialising...
iteration: 0 ; trust region: 4.000000000000001e-06 ; memory: -1
iteration: 1 ; trust region: 40.00000000000001 ; memory: 0
iteration: 2 ; trust region: 40.00000000000001 ; memory: 1
iteration: 3 ; trust region: 200.00000000000003 ; memory: 2
iteration: 4 ; trust region: 1000.0000000000001 ; memory: 3
iteration: 5 ; trust region: 5000.000000000001 ; memory: 4
iteration: 6 ; trust region: 25000.000000000004 ; memory: 5
iteration: 7 ; trust region: 125000.00000000001 ; memory: 6
iteration: 8 ; trust region: 625000.0000000001 ; memory: 7
iteration: 9 ; trust region: 3125000.0000000005 ; memory: 7
iteration: 10 ; trust region: 15625000.000000002 ; memory: 7
iteration: 11 ; trust region: 78125000.00000001 ; memory: 7
```

The last step in using Samsara is to output the results

```python
if stepsize <= options['step_TOL']:
    print('Algorithm stagnated:  stepsize tolerance violated.')
if it >= options['Maxit']:
    print('Algorithm exceeded:  maximum step count violated.')

print('iterations:', it, '; optimal value:', fnew)
```

    iterations: 12 ; optimal value: 3.1086244689504383e-15

and to plot the behavior of the function values.

```python
samsara.plot(fopt=fnew)
plt.savefig('./samsara_results.png')
'./samsara_results.png'
```

![img](./samsara_results.png)


<a id="org392e1bb"></a>

## Credits

Development on SAMSARA began in 2007 with funding from the National Science Foundation of the USA, DMS-0712796.

Contributors include:

-   Russell Luke (main author), Institute for Numerical and Applied Mathematics, University of Göttingen

-   Student helpers:
    -   Rachael Bailine (Matlab and Fortran version), University of Delaware
    -   Patrick Rowe (Fortran version), University of Delaware
    -   Brian Rife (Fortran version), University of Delaware
    -   Marco Bedolla (Fortran version), University of Delaware
    -   Benedikt Rascher-Friesenhausen (Python version), University of Göttingen

Special thanks to Laurence Marks at Northwestern University and Peter Blaha at the Technical University of Vienna who provided much of the inspiration for SAMSARA.
